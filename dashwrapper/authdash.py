import dash
from flask_security import login_required

'''
We define a sublass of the Dash app class which has its index method wrapped
with a login_required decorator. The index method is the main view used by Dash.
This will cause a redirect to the login page if a user that is not logged in
tries to access the dashboard.

At the moment this does not prevent access to other URLs provided by the Dash
app such as {base_path}_dash-routes. These should possibly be protected as well,
but the effect of the decorator needs to be tested on each of their views.
'''
class AuthDash(dash.Dash):
    index = login_required(dash.Dash.index)
