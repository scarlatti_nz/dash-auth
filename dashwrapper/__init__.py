import os

from flask import Flask, session
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_security import Security, SQLAlchemyUserDatastore,login_required
from .database import db
from .models import User, Role
from .dashboard import create_dash

# App factory function
def create_app(test_config=None):
    # create and configure the app
    app_flask = Flask(__name__, instance_relative_config=True)
    # ensure the instance folder exists
    try:
        os.makedirs(app_flask.instance_path)
    except OSError:
        pass
    app_flask.config.from_object(__name__+'.default_settings')
    app_flask.config.from_pyfile('settings.cfg')
    # Local SQlite testing database path
    app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+os.path.join(app_flask.instance_path,'test.db')

    db.init_app(app_flask)

    # Setup Flask-Security
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security = Security(app_flask, user_datastore)

    # Setup Flask-Admin
    admin = Admin(app_flask)
    admin.add_view(ModelView(User,db.session))
    admin.add_view(ModelView(Role,db.session))


    # Views
    # Bluprints

    # Dash
    app_dash = create_dash(__name__,server=app_flask,url_base_pathname='/dash/')

    # Homepage
    @app_flask.route('/')
    @login_required
    def home():
        current_user = User.query.get(session["user_id"])
        return 'Welcome ' + current_user.email + '. You are now logged in.'



    return app_flask
