from flask_sqlalchemy import SQLAlchemy

'''
Any database configuration can go here
'''

# Create database connection object
db = SQLAlchemy()
