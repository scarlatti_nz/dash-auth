from flask import session

from .authdash import AuthDash
from .models import User

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

# This function returns the dash app object for use in the flask app factory function in __init__.py
def create_dash(name,server,url_base_pathname):
    app_dash = AuthDash(name, server=server, url_base_pathname=url_base_pathname)

    # Create your dashboard here
    app_dash.layout = html.Div([
      html.Div(id='my-div'),
      dcc.Input(id='my-id', value='initial value', type='text'),
    ])


    @app_dash.callback(
      Output(component_id='my-div', component_property='children'),
      [Input(component_id='my-id', component_property='value')]
    )
    def update_output_div(input_value):
      current_user = User.query.get(session["user_id"])
      return 'Hi {}. You wrote "{}"'.format(current_user.email,input_value)

    return app_dash
